Sinatra
=============

Brought to you by Lighthouse Labs

## Getting Started

1. `bundle install`
2. `shotgun -p 3000 -o 0.0.0.0`
3. Visit `http://localhost:3000/` in your browser

###To get paperclip to work

If you are running the test suite, you'll also need to install GhostScript. On Mac OS X, you can also install that using Homebrew:

brew install gs

If you are on Ubuntu (or any Debian base Linux distribution), you'll want to run the following with apt-get:

sudo apt-get install imagemagick -y
